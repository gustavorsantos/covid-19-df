# Monitoramento COVID-19

Esse projeto tem o intuito de fornecer em tempo real* informações sobre casos do COVID-19 na região do Distrito Federal e Entorno. 

Os dados estão separados por:
* Sexo
    * Masculino
    * Feminino
* Idade
    * Menor que 2 anos
    * 2 a 10 anos
    * 11 a 19 anos
    * 20 a 49 anos
    * 50 a 59 anos
    * 60 a 69 anos
    * 70 a 79 anos
    * 80 anos ou mais
* Região Administrativa
    * Águas Claras
    * Recanto Das Emas
    * Samambaia
    * Taguatinga
    * Vicente Pires
    * Plano Piloto
    * Sudoeste/Octogonal
    * Cruzeiro
    * Lago Norte
    * Lago Sul
    * Varjão Do Torto
    * Candangolândia
    * Parkway
    * Guará
    * Núcleo Bandeirante
    * Riacho Fundo I
    * Riacho Fundo II
    * SCIA (Estrutural)
    * SAAN
    * Fercal
    * Planaltina
    * Sobradinho I
    * Sobradinho II
    * Gama
    * Santa Maria
    * Jardim Botânico
    * Itapoã
    * Paranoá
    * São Sebastião
    * Brazlândia
    * Ceilândia



*Não é exatamente em tempo real uma vez que os boletins com os dados, fornecidos pela Secretaria de Saúde do DF, são publicados apenas diariamente, para fins de monitoramento em tempo real, o ideal seria que fossem divulgados novos boletins a cada hora.