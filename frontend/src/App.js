import React from 'react'
import Mapa from './Components/Mapa'
import Quadro from './Components/Quadro'
import './App.css'

function App() {

  return (
    <div className="App">
      <div className="left-app">
        <Quadro/>
      </div>
      <div className="right-app">
        <Mapa/>
        <div id="sobre" className="wobble-hor-bottom">
          <a href="/sobre"> <p className="i">i</p></a>
        </div>
      </div>
    </div>

  );
}

export default App;
