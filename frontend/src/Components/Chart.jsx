import React, {Component} from 'react'
import axios from 'axios'
import {Line} from 'react-chartjs-2'
import array from '../scripts/arrayDias'
import variable from '../scripts/vars'
import {sub} from './Observable'

class Chart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            chartData: {
                labels: array.getArray(this.props.dias),
                datasets:[{
                    data: array.inicialArray(this.props.dias),
                    borderColor: 'rgba(244,81,28,0.8)',
                    backgroundColor: 'rgba(244,81,28,0.4)',
                    pointBackgroundColor: 'rgba(244,81,28,1)',
                    pointBorderColor: 'rgba(244,81,28,1)'
                }]
            },
            regiaoGrafico: ''
        }
    }
    componentDidMount() {
        let url = variable['ADDRESS']+'/dado/grafico'
        let n_dias = this.props.dias
        let grupo = this.props.grupo

        if(this.props.grupo === 'ra' && this.props.target !== 'PopulaçãoPrivadadeLiberdade' ){
            //url = url+'?dias='+n_dias+'&grupo='+grupo+'&target='
            
            sub.subscribe((v) => {
                let {regiaoGrafico} = this.state
                regiaoGrafico = ''
                regiaoGrafico = v
                this.setState({regiaoGrafico})

                if(this.state.regiaoGrafico === 'Sobradinho' || this.state.regiaoGrafico === 'Riacho Fundo')
                    this.setState({regiaoGrafico: this.state.regiaoGrafico.concat('I')})
                else if(this.state.regiaoGrafico === 'Varjão')
                    this.setState({regiaoGrafico: 'VarjãodoTorto'})
                else if(this.state.regiaoGrafico === 'Park Way')
                    this.setState({regiaoGrafico: 'Park way'})
                else if(this.state.regiaoGrafico === 'Recanto das Emas')
                    this.setState({regiaoGrafico: 'Recanto Das Emas'})
                else if(this.state.regiaoGrafico === 'SIA')
                    this.setState({regiaoGrafico: 'SAAN'})
                else if(this.state.regiaoGrafico === 'SCIA')
                    this.setState({regiaoGrafico: 'SCIA (Estrutural)'})
                else if(this.state.regiaoGrafico === 'Pôr do Sol')
                    this.setState({regiaoGrafico: 'Sol Nascente'})
                else if(this.state.regiaoGrafico === 'Arniqueira')
                    this.setState({
                        chartData:{
                            datasets:[{
                                data : array.inicialArray(this.props.dias)
                            }]
                        }
                    })

                if(this.state.regiaoGrafico !== '') {
                    url = url+'?dias='+n_dias+'&grupo='+grupo+'&target='
                    url = url+this.state.regiaoGrafico.replace(/\s/g, "")
                    console.log("A"+url)    
                    axios.get(url).then(res => {
                        this.setState({
                            chartData:{
                                datasets:[{
                                    data : res.data
                                }]
                            }})
                    })
                    url = variable['ADDRESS']+'/dado/grafico'
                }

            })
        }else {
            if(this.props.target == null) {
                url = url+'?dias='+this.props.dias+'&grupo='+this.props.grupo
            }else{
                url = url+'?dias='+this.props.dias+'&grupo='+this.props.grupo+'&target='+this.props.target
            }

            axios.get(url).then(res => {
                this.setState({
                    chartData:{
                        datasets:[{
                            data : res.data
                        }]
                    }})
            })
        }
        
        
        
    }
    render() {
        return (
            <div className="chart">
                <Line
                    data={this.state.chartData}
                    options={{
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        },
                        legend: {
                            display: false
                        },
                        title: {
                            display: true, 
                            text: ['Número de casos nos últimos '+this.props.dias+' dias', (this.props.target === undefined ? (this.props.grupo.charAt(0).toUpperCase() + this.props.grupo.slice(1)) : this.props.target.replace(/([A-Z])/g, ' $1').trim())],
                            fontFamily: "'Roboto', sans-serif"
                        },
                        responsive: true
                    }}
                />
            </div>
        )
    }

}

export default Chart