import React, {Component} from 'react'
import axios from 'axios'
import {Pie} from 'react-chartjs-2'
import date from '../scripts/Date'
import variable from '../scripts/vars'

class PieChart extends Component {
    constructor(props) {
        super(props)
        this.state = {
            chartData: {
                labels: [],
                datasets:[{
                    data: [],
                    borderColor: 'rgba(244,81,28,0)',
                    backgroundColor: ['#FF6633', '#FFB399', '#FF33FF', '#FFFF99', '#00B3E6', 
                    '#E6B333', '#3366E6', '#999966', '#99FF99', '#B34D4D',
                    '#80B300', '#809900', '#E6B3B3', '#6680B3', '#66991A', 
                    '#FF99E6', '#CCFF1A', '#FF1A66', '#E6331A', '#33FFCC',
                    '#66994D', '#B366CC', '#4D8000', '#B33300', '#CC80CC', 
                    '#66664D', '#991AFF', '#E666FF', '#4DB3FF', '#1AB399',
                    '#E666B3', '#33991A', '#CC9999', '#B3B31A', '#00E680', 
                    '#4D8066', '#809980', '#E6FF80', '#1AFF33', '#999933',
                    '#FF3380', '#CCCC00', '#66E64D', '#4D80CC', '#9900B3', 
                    '#E64D66', '#4DB380', '#FF4D4D', '#99E6E6', '#6666FF'],
                }]
            }
        }
    }
    componentDidMount() {
        
        let url = variable['ADDRESS']+'/dado/' + date.getCurrentDate()
        
        if(this.props.target == null) {
            url = url+'/'+this.props.grupo
        }else{
            url = url+'/'+this.props.grupo+'?sub='+this.props.target
        }
        
        axios.get(url).then(res => {
            let obj = Object.keys(res.data)
            let values = []
            for(let i = 0; i < obj.length; i++) {
                values.push(res.data[obj[i]])
                if(this.props.grupo === 'idade') {
                    obj[i] = obj[i].replace('80mais', '≥ 80')
                    obj[i] = obj[i].replace('2menos', '< 2')    
                    obj[i] = obj[i].replace('a', ' a ')
                }
            }
            this.setState({
                chartData:{
                    labels: obj,
                    datasets:[{
                        data : values
                    }]
                }})
        })
    }
    render() {
        return (
            <div className="chart">
                <Pie
                    data={this.state.chartData}
                    options={{
                        legend: {
                            display: true,
                            position: 'right',
                            onHover: function(e, element) {
                                return 'oi'
                            },
                            labels: {
                                padding: 0,
                                boxWidth: 3,    
                            }
                        },
                        title: {
                            display: true, 
                            text: ['Distribuição de casos por ' + this.props.grupo],
                            fontFamily: "'Roboto', sans-serif"
                        },
                        responsive: true
                    }}
                />
            </div>
        )
    }

}

export default PieChart