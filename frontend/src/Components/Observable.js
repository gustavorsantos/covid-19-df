import {BehaviorSubject} from 'rxjs'

const sub = new BehaviorSubject('Selecione uma região')

const mudaRegiao = {
    send: function (r) { 
        sub.next(r)
    }
}

export { 
    mudaRegiao,
    sub
}