import React, { Component } from 'react'
import axios from 'axios'       
import date from '../scripts/Date'
import virus from '../assets/icons/virus.svg'
import Chart from './Chart'
import PieChart from './PieChart'
import {sub} from './Observable'
import './Quadro.css'
import variable from '../scripts/vars'

export default class Quadro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentDate: date.getCurrentDate(),
            data: {},
            regiaoGrafico: ''
        }
    }
    componentDidMount() {
        const url = variable['ADDRESS']+"/dado/" + this.state.currentDate
        axios.get(url).then(res => {
            this.setState({data: res.data})
        })

        sub.subscribe((v) => {
            let {regiaoGrafico} = this.state
            regiaoGrafico = v
            this.setState({regiaoGrafico})
        })
    }

    render () {
        return (
            <div className='quadro'>
                <div className="box">
                    <img src={virus} alt="" className="icon"/>
                    <p className="legenda">TOTAL</p>
                    <p className="num-total">{this.state.data.total}</p>
                    <div className="grid">
                        <span className="left">
                            <p className="num">{this.state.data.sexo?this.state.data.sexo.feminino:""}</p>
                            <p className="legenda">MULHERES</p>
                        </span>
                        <span className="right">
                            <p className="num">{this.state.data.sexo?this.state.data.sexo.masculino:""}</p>
                            <p className="legenda">HOMENS</p>
                        </span>
                    </div>
                </div>
                <div className="box">
                    <div className="grid">
                        <div className="left">
                            <p className="legenda">RECUPERADOS*</p>
                            <p className="num">{this.state.data.recuperados}</p>
                        </div>
                        <div className="right">
                            <p className="legenda">ÓBITOS</p>
                            <p className="num">{this.state.data.obitos}</p>
                        </div>
                    </div>
                    <p className="obs">*caso confirmado laboratorialmente, com mais de 14 dias de sintomas que
                    não está hospitalizado e não evoluiu à óbito.</p>
                </div>
                <div className="break"></div>
                <div className="box">
                    <div>{this.state.regiaoGrafico}</div>
                    <Chart dias='30' grupo='ra' target=''></Chart>
                </div>
                <div className="break"></div>
                <div className="box"><Chart dias='30' grupo='ra' target="PopulaçãoPrivadadeLiberdade"></Chart></div>
                <div className="box"><PieChart grupo='idade'></PieChart></div>
                <div className="break"></div>
                <div className="box"><Chart dias='61' grupo='total'></Chart></div>
                <div className="break"></div>
                <div className="box"><Chart dias='30' grupo='obitos'></Chart></div>
                
            </div>
        )
    }

}