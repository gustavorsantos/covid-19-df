import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import L from "leaflet"
import 'leaflet/dist/leaflet.css'
import "./Mapa.css"
import geojson from '../scripts/geojson'
import {sub} from './Observable'

export default class Mapa extends Component {
    
    style() {
        return {
            fillColor: '#31303a',
            weight: 1.4,
            opacity: 1,
            color: '#31303a',
            fillOpacity: 0.2
        }
    }

    componentDidMount() {   
        let gData = geojson.getJSON()
        let map = ReactDOM.findDOMNode(this)

        //Cria o mapa
        const mymap = this.mapaid = L.map(map, {minZoom: 9, zoomControl: false,maxBounds: [[-15.077, -48.774],[-16.684, -46.708]]}).setView([-15.7936, -47.8262], 10)
        
        // Adiciona o mapa base
        //https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png
        L.tileLayer('https://{s}.basemaps.cartocdn.com/light_nolabels/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mymap)

        // Adiciona os polígonos de cada região
        L.geoJson(gData, {style: this.style}, {onEachFeature: this.onEachFeature}).addTo(mymap)

        //console.log(gData.features[31].properties.ra)
        
        //Cria a div para mostrar o nome das regiões 
        var info = L.control()
        info.onAdd = function (mymap) {
            this._div = L.DomUtil.create('div', 'info') // create a div with a class "info"
            this.update()
            return this._div
        }
        let isClicked = false
        let layerSelect = "none"
        //Execução de cada evento do mousa para camada
        mymap.eachLayer(function(layer) {
            
            //Clique
            layer.on('click', function(e){
                //Resgata o nome da camada e envia para mudar dinamicamente o gráfico
                let regiaoAtual = e.layer?e.layer.feature.properties.ra:""
                sub.next(regiaoAtual)

                if(e.layer !== undefined) {
                    layerSelect = e.layer.feature.properties.ra 

                    if(isClicked)
                        this.setStyle({ fillOpacity: 0.2})

                    e.layer.setStyle({ fillOpacity: 1})
                    isClicked = true
                }
            })

            //Mouse sobre
            layer.on('mouseover', function(e){
                //Atualiza o nome na div
                info.update = function (props) {
                    this._div.innerHTML = e.layer?e.layer.feature.properties.ra:""
                }
                info.addTo(mymap)

                if(e.layer !== undefined) {
                    if(e.layer.feature.properties.ra !== layerSelect)
                        e.layer.setStyle({ fillOpacity: 1})
                }
            })

            //Saída do mouse
            layer.on('mouseout', function(e) {
                if(e.layer !== undefined) {
                    if(!isClicked)
                        e.layer.setStyle({ fillOpacity: 0.2})
                    else {
                        if(e.layer.feature.properties.ra !== layerSelect)
                            e.layer.setStyle({ fillOpacity: 0.2})
                    }
                }
            })
        })
        
    }
    render () {
        return (
            <div className='mapaid'></div>
        )
    }

}