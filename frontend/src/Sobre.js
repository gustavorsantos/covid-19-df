import React from 'react';
import './Sobre.css'
function Sobre() {
    return (
        <div>
            <div className="header">
                <h1 className="title">Monitoramento COVID-19 - Distrito Federal</h1>
            </div>
            <div className="content">
                <h2 className="sub">Como funciona?</h2>
                <p className="p">
                    Os dados são extraídos a partir dos <b>
                    <a href="http://www.saude.df.gov.br/boletinsinformativos-divep-cieves/" target="_blank" rel="noopener noreferrer">boletins diários</a> </b> 
                    da <b>Secretaria de Saúde do Distrito Federal.</b> Após isso, ocorre um 
                    processamento para que as informações sejam guardadas num arquivo JSON. Por fim,
                    ao finalizar o processamento os dados estão prontos para serem exibidos.
                </p>
                <h2 className="sub">Sobre os Dados</h2>
                <p className="p">
                    Os dados são extraídos a partir de arquivos pdf, porém há uma certa 
                    dificuldade ao extrair dados de pdf por este motivo os dados em formato
                    JSON são disponibilizados na paǵina <a href="/dados">Dados</a>.
                </p>
                <h2 className="sub">Tempo Real</h2>
                <p className="p">
                    As informações não são em tempo real pois a Secretaria de Saúde apenas libera 
                    os boletins diariamente. Dessa forma, se você está acessando antes das 20 horas
                    os dados exibidos são os do dia anterior, somente após as 20, o banco de dados é 
                    atualizado e as informações referentes aquele dia são exibidas.
                </p>
                <h2 className="sub">Dados inconsistentes</h2>
                <p className="p">
                    Como dito anteriormente, as informações são extraídas a partir de um relatório, 
                    gravado em pdf. Para a extração das informações é utilizado um algoritmo onde são
                    utilizadas palavras chaves para encontrar os dados, porém por se tratar de um relatório
                    as palavras e as sentenças podem ser mudadas de acordo com escrita do documento, ocasionando
                    erro na busca dos dados. E consequentemente armazenando dados errados. <br></br>
                    <b>Se você encontrou um erro, favor reportar em teste@teste.com</b>.
                </p>
            </div>
      </div>
    );
  }
  
export default Sobre;