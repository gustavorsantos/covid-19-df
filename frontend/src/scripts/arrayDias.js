let array = {
    getArray(dias) {
        const dataAtual = new Date()
        if(dataAtual.getHours() < 20)
            dataAtual.setDate(dataAtual.getDate() - 1)
        let arrayDias = []

        for(let dia = 0; dia < dias; dia++) {
            let diff = new Date()
            diff.setDate(dataAtual.getDate() - dia)
            let item = diff.getDate().toString() + "/" + (diff.getMonth()+1).toString()
            arrayDias.push(item)
        }
        
        return arrayDias.reverse()

    },

    inicialArray(dias) {
        let arrayDias = []
        for(let dia = 0; dia < dias; dia++) {
            arrayDias.push(0)
        }
        return arrayDias
    }
}

export default array