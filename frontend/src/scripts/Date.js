let date = {
    getCurrentDate() {
        const d = new Date()
        let day = d.getDate() 
        let month = d.getMonth() + 1 //retorna valor de 0 a 11
        let year = d.getFullYear()
    
        if(d.getHours() < 20){
            d.setDate(d.getDate() - 1)
            day = d.getDate() 
            month = d.getMonth() + 1
            year = d.getFullYear()
            return day + '/' + month + '/' + year
        }
        else
            return day + '/' + month + '/' + year
    }
}

export default date