import json
from flask import Flask, request, Response
from datetime import datetime, timedelta, date

app = Flask(__name__)

@app.route('/dado/grafico', methods=['GET'])   
def dadosGrafico():
    dias = request.args.get('dias')
    grupo = request.args.get('grupo')
    target = request.args.get('target')

    if(datetime.now().hour < 20):
        dataAtual = date(datetime.now().year, datetime.now().month, datetime.now().day) - timedelta(1)
    else:
        dataAtual = date(datetime.now().year, datetime.now().month, datetime.now().day)


    arrayDias = []

    for dia in range (int(dias)):
        difference = dataAtual - timedelta(dia)
        name = str(difference.day)+'_'+str(difference.month)+'_'+str(difference.year)
        file = "dados_" + name + ".json"
        print("ABRINDO: ""dados/" + str(difference.month) + '_'+ str(difference.year) + '/' + file)
        with open("dados/" + str(difference.month) + '_'+ str(difference.year) + '/' + file) as f:
            data = json.load(f)
        if(target == None):
            arrayDias.append(data[grupo])
        else:
            if(target in data[grupo]):
                arrayDias.append(data[grupo][target])
            else:
                arrayDias.append(0)

    print(arrayDias[::-1])
    response = app.response_class(
        response=json.dumps(arrayDias[::-1]),
        status=200,
        mimetype='application/json'
    )
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/dado/<dia>/<mes>/<ano>/<grupo>', methods=['GET'])   
def dadoDia(dia, mes, ano, grupo):
    file = "dados_" + dia + "_" + mes + "_" + ano + ".json" 
    print(file)
    sub = request.args.get('target')
    with open("dados/"+ mes + '_' + ano + '/' + file) as f:
        data = json.load(f)
    if(sub == None):
        response = app.response_class(
            response=json.dumps(data[grupo], indent=4),
            status=200,
            mimetype='application/json'
        )
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response
    else:
        response = app.response_class(
            response=json.dumps(data[grupo][sub], indent=4),
            status=200,
            mimetype='application/json'
        )
        response.headers.add('Access-Control-Allow-Origin', '*')
        return response

@app.route('/dado/<dia>/<mes>/<ano>', methods=['GET'])
def dia(dia, mes, ano):
    file = "dados_" + dia + "_" + mes + "_" + ano + ".json"
    with open("dados/"+ mes + '_' + ano + '/' + file) as f:
        data = json.load(f)

    response = app.response_class(
        response=json.dumps(data, indent = 4),
        mimetype='application/json'
    )
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/')
def main():
    return("Covid API")
    

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')