#!/bin/bash
eval $(conda shell.bash hook)

cd /home/gustavo/Documents/covid-19-df/backend
source /home/gustavo/miniconda3/etc/profile.d/conda.sh
conda activate covid
/home/gustavo/miniconda3/envs/covid/bin/python script.py

cd download
rm -rf *