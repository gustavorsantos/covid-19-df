# Backend

## Preparando para desenvolvimento

### Download 
Escolha um diretório para alocar o projeto e execute:
```
git clone https://gitlab.com/gustavorsantos/covid-19-df.git
```
### Miniconda 
O Miniconda é um gerenciador de pacotes e dependências do python. 
Acesse o site do [Miniconda](https://docs.conda.io/en/latest/miniconda.html) baixe o instalador do Linux.

Para executar o .sh
```
./miniconda3-latest-Linux.sh
```
#### Ambiente virtual
O ambiente virtual ajuda a separar os pacotes instalados para cada aplicação.
```
cd pasta_onde_foi_baixado/covid-19-df/backend
conda env create --name covid -f env.yml  #Esse comando baixa automáticamente os pacotes necessários
```
```
conda activate covid        #para ativar o ambiente
conda deactivate covid      #para desativar
```

**Sempre** que for executar algum script do backend deve-se **ativar o ambiente**.

## Funcionamento - Backend

### Mineração de dados
O arquivo script.py contém o script que realiza a extração dos dados. Inicialmente, acessa-se o site onde são publicados os **Boletins** diários, porém foi notado que não há um padrão de URL dos dados. 

#### 1. Download do boletim
Dessa forma, foi instalado o *BeatifulSoup* para realizar um HTML Parse (1) que permite transformar o HTML da página em uma string e pegar o primeiro link. 

Após pegar o link é feita uma requisição para baixar o arquivo (2) que será salvo na pasta /downloads. 

#### 2. Conversão do arquivo
Entretanto deve ser realizado a conversão (3) de pdf para txt para o python conseguir ler, e dessa forma foi escolhido o *Tika* para realizar a conversão, o arquivo é salvo na mesma pasta. 

#### 3. Extração dos dados
Em seguida da conversão, é realizada a extração dos dados através da função getData(p1, p2, p3, p4), onde:
* p1 - string antes do dado
* p2 - string após o dado
* p3 - Propriedade do JSON
* p4 - Subpropriedade do JSON

Ex: no boletim para achar o número de casos, há sempre a presença da sentença "foram confirmados 370 casos de COVID-19", dessa forma 
* p1 = 'foram confirmados '
* p2 = ' casos'
* p3 = total
* p4 = None     #pois não há subpropriedade

OBS: Em alguns casos, há a necessidade de remover os espaços em branco do arquivo inteiro para conseguir resutados mais acertivos.

#### 4. Escrita dos dado num arquivo JSON
Foi escolhido o formato JSON pois é um tipo arquivo de fácil leitura e estruturação. 

A função writeJSON() realiza a escrita dos dados no arquivo, com base no arquivo *modelo.json*.

Pretende-se que o site seja também um repositório de dados para consumo público.

#### 5. Cronjob
Abre o arquivo cron.txt e copie o conteúdo.

Abra o terminal e digite:
```
crontab -e
```
Cole o conteúdo copiado anteriormente no final do arquivo.

Pronto, a tarefa já programada e será executada todo dia às 20:00.

### API
A api foi baseado no microframework **Flask**.

O funcionamento é simples, na URL /dia/mes/ano há o retorno do arquivo JSON todo de determinado dia e na outra URL /dia/mes/ano/propriedade?target=subprop é retornado apenas o valor associado com essas chaves.

Para executar o backend 
```
cd backend
python app.py runserve      #Ctrl+C para finalizar
```
O serviço será iniciado no localhost:5000. 
Ao acessar o endereço deverá ser exibido "Covid API". 

Realize alguns teste para ver se os dados estão sendo entregues corretamente e confira de acordo com o JSON.
Ex: /18/4/2020/sexo?target=feminino, /18/4/2020/total

