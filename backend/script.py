# -*- coding: utf-8 -*-
import re
import os
import json
import requests
import datetime 
import shutil
from tika import parser
from bs4 import BeautifulSoup

def getDate():
    dt = datetime.datetime.today()
    dateString = str(dt.day) + '_' + str(dt.month) + '_' + str(dt.year)
    return dateString

def getMonth():
    date = getDate()
    d = date.split('_')[1] + '_' +date.split('_')[2] 
    return d

def getTime():
    now = datetime.datetime.now()
    current_time = now.strftime("%H:%M:%S")
    return current_time

def downloadPDF(url):
    date = getDate()
    _dir = os.getcwd() + '/download'
    file = _dir + '/relatorio_' + date + '.pdf'

    
    r = requests.get(url, stream=True)
    if not os.path.exists(_dir):
        os.mkdir(_dir)

    with open(file, 'wb') as f:
        f.write(r.content)

    return file

def getHtml():
    URL = 'http://www.saude.df.gov.br/informativos-do-centro-de-operacoes-de-emergencia-coe/'
    r = requests.get(URL)
    html = r.text
    soup = BeautifulSoup(html)

    link = soup.find('a', href=re.compile('^http://www.saude.df.gov.br/wp-conteudo'))['href']
    return link

def convertTXT(file):
    parsedPDF = parser.from_file(file)
    textcomp = re.sub(r"\s+", "", parsedPDF["content"])
    with open(file.split('.')[0]+'.txt', 'w') as f:
        f.write(parsedPDF["content"])

def writeJSON(obj, attr, value):
    dir = getMonth()
    dir_path = os.getcwd() + '/dados/'+ dir
    json_path = os.getcwd() + '/dados/'+ dir +'/dados_'+getDate()+'.json'
    json_base = os.getcwd() + '/dados/modelo.json'
    
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    if not os.path.exists(json_path):
        shutil.copy(json_base, json_path)
        
    with open(json_path, 'r+') as f:
        data = json.load(f)
        if attr == None:
            data[obj] = value # <--- add `id` value.
        else:
            data[obj][attr] = value
        f.seek(0)        # <--- should reset file position to the beginning.
        json.dump(data, f, indent=4)
        f.truncate()     # remove remaining part

def getData(string, start, end):
    data = string.split(start)[1].split(end)[0]
    data = data.replace('.', '')
    #print ("dadasd"+data)
    return int (data)

def extract(file):
    file = file.split('.')[0] + '.txt'
    with open(file, 'r') as txt:
        text = txt.read()
    textcomp = re.sub(r"\s+", "", text) # Retira todos os ' ' do texto
    
    # TOTAL DE CASOS
    total = getData(textcomp, 'foramconfirmados1', 'casosdeCOVID') #total
    writeJSON('total', None, total)

    # POR SEXO MASC. E FEMIN.
    homens = getData(textcomp, 'Doscasosconfirmados,', '(')
    writeJSON('sexo', 'masculino', homens)
    writeJSON('sexo', 'feminino',(total - homens))

    # RECUPERADOS
    recuperado = getData(textcomp, 'identificou-seque', '(') #recuperados
    writeJSON('recuperados', None, recuperado)

    # ÓBITOS
    obito = getData(textcomp, 'Intensiva,e', 'foramaóbito') #obitos
    writeJSON('obitos', None, obito)

    with open("dados/modelo.json") as file:
        data = json.load(file)

    #POR IDADE
    idades = data['idade'].keys()
    for faixa in idades:
        faixa_procurada = ''
        
        if(faixa == '2menos'):
            faixa_procurada = 'Menor de 2'
        elif (faixa == '80mais'):
            faixa_procurada = '80 e mais'
        else:
            faixa_procurada = faixa.replace('a', ' a ')

        num_faixa = getData(text.split("segundo faixa etária")[1], faixa_procurada+' ', ' ')
        writeJSON('idade', faixa, num_faixa)
    
    #POR REGIÃO ADMINISTRATIVAS  
    regioes = data['ra'].keys()
    for regiao in regioes:
        regiao_procurada = ''
        str_final = ' '
        if(regiao == 'SCIA(Estrutural)'):
            regiao_procurada = 'SCIA (Estrutural)'
        elif(regiao == 'SAAN'):
            regiao_procurada = regiao
        elif (regiao == 'VarjãodoTorto'):
            regiao_procurada = 'Varjão do Torto'
        elif (regiao == 'PopulaçãoPrivadadeLiberdade'):
            regiao_procurada = 'População Privada de Liberdade'
        elif (regiao == 'Ceilândia'):
            regiao_procurada = regiao_procurada + '****'
        else:
            regiao_procurada = re.sub(r"(\w)([A-Z])", r"\1 \2", regiao)

        if(regiao_procurada == 'Sol Nascente'):
            num_regiao = getData(text.split("Total DF")[1], ' (', ') dos casos da RA Sol Nascente')
            writeJSON('ra', regiao, num_regiao)
        else:
            #print(regiao_procurada)
            num_regiao = getData(text.split("Tabela 2.")[1], regiao_procurada+' ', ' ')
            writeJSON('ra', regiao, num_regiao)


link = getHtml()
path = downloadPDF(link)
convertTXT(path)
extract(path)
print("Todos os dados obtidos com sucesso!")